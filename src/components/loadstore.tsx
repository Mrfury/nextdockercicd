import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { setProducts } from "@/store/slice";
function LoadStore({ children }: { children: React.ReactNode }) {
  console.log("loadstore is rendering ...... ");
  const dispatch = useDispatch();
  useEffect(() => {
    const products = JSON.parse(localStorage.getItem("products") || "null");
    console.log({ products });
    if (products) {
      dispatch(setProducts(products));
    }
  }, []);
  return <>{children}</>;
}

export default LoadStore;
