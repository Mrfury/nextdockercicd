import { useSelector } from "react-redux";
import { RootState } from "@/store/store";
function Cart() {
  const products = useSelector<RootState, number[]>(
    (state) => state.cartSlice.value.products
  );
  return (
    <div>
      {products.map((element) => (
        <h1 key={element}>{element}</h1>
      ))}
    </div>
  );
}

export default Cart;
