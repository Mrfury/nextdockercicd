"use client";
import { useSearchParams } from "next/navigation";
function Login() {
  const searchParams = useSearchParams();
  const callBackUrl = searchParams.get("callBackUrl");
  console.log({ callBackUrl });
  console.log("rendering login");
  return (
    <div>
      <input type="text" placeholder="username" />
      <input type="password" placeholder="password" />
      <button
        type="button"
        onClick={async () => {
          const result = await fetch("http://localhost:4000/login", {
            method: "POST",
          });
          const data = await result.json();
          console.log({ data });
          document.cookie = `token=${data.token};`;
          location.href = "http://localhost:3000" + callBackUrl;
        }}
      >
        Login
      </button>
    </div>
  );
}

export default Login;
