import { cookies } from "next/headers";
import { redirect } from "next/navigation";
async function Profile() {
  console.log("rendering profile!");
  const cookieStore = cookies();
  const token = cookieStore.get("token");
  if (!token) return redirect("/login?callBackUrl=/profile");
  const result = await fetch("http://localhost:4000/me");
  const data = await result.json();
  console.log({ data });
  return <h1>this is {data.user.name}</h1>;
}

export default Profile;
