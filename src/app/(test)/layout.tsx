"use client";
import { Provider } from "react-redux";
import store from "@/store/store";
import LoadStore from "@/components/loadstore";
function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div>
      <Provider store={store}>
        <LoadStore>{children}</LoadStore>
      </Provider>
    </div>
  );
}

export default Layout;
