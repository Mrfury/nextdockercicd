"use client";
import { addProducts } from "@/store/slice";
import { useDispatch } from "react-redux";
import Link from "next/link";
function Home() {
  const dispatch = useDispatch();

  return (
    <div>
      <div
        onClick={(event) => {
          dispatch(addProducts(1));
        }}
      >
        product 1
      </div>
      <div>product 2</div>
      <div>product 3</div>
      <Link href="/cart">Cart</Link>
    </div>
  );
}

export default Home;
