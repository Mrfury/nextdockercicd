import { createSlice } from "@reduxjs/toolkit";
type sliceType = {
  value: {
    products: number[];
  };
};
const initialState: sliceType = {
  value: {
    products: [],
  },
};

export const cartSlice = createSlice({
  name: "cartSlice",
  initialState,
  reducers: {
    addProducts: (state, action) => {
      state.value.products.push(action.payload);
    },
    setProducts: (state, action) => {
      state.value.products = action.payload;
    },
  },
});

export const { addProducts, setProducts } = cartSlice.actions;

export default cartSlice.reducer;
