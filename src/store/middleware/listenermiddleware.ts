import { createListenerMiddleware, isAnyOf } from "@reduxjs/toolkit";
import { addProducts } from "@/store/slice";
import type { RootState } from "@/store/store";

export const listenerMiddleware = createListenerMiddleware();
listenerMiddleware.startListening({
  matcher: isAnyOf(addProducts),
  effect: (action, listenerApi) =>
    localStorage.setItem(
      "products",
      JSON.stringify(
        (listenerApi.getState() as RootState).cartSlice.value.products
      )
    ),
});
