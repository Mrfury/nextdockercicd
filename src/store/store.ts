import { configureStore } from "@reduxjs/toolkit";
import cartSlice from "./slice";
import { listenerMiddleware } from "./middleware/listenermiddleware";

// const productState = JSON.parse(localStorage.getItem("products") || "null");

const store = configureStore({
  // preloadedState: {
  //   products: productState === null ? { products: [] } : productState,
  // },
  reducer: { cartSlice },
  middleware: (getDefaultMiddleware) => [
    ...getDefaultMiddleware(),
    listenerMiddleware.middleware,
  ],
});

export type RootState = ReturnType<typeof store.getState>;

export default store;
