const express = require("express");
const cors = require("cors");
const app = express();
const port = 4000;

app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/me", (req, res) => {
  res.json({
    user: {
      userName: "something",
      name: "another",
    },
  });
});

app.post("/login", (req, res) => {
  res.json({
    token: "anotherthing",
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
